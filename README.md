# LivingEarth LCCS

| | |
|-|-|
|__Disclaimer__| This repository is *in development*, use at your own risk |
|__License__| The Apache 2.0 license applies to this open source code. |


### About
LivingEarth LCCS is derived from the EODESM system, based on [The Earth Obsevation Data Ecosystem Monitoring (Eodesm) System](https://ieeexplore.ieee.org/abstract/document/8519474/references#references) paper, with a focus on optimizing efficiency and processing speeds, and allowing for integrated use within Digital Earth Australia (DEA) and the broader Open Data Cube (ODC) environment and API.


### Getting Started
``` git clone git@bitbucket.org:au-eoed/livingearth_lccs.git```


### Development

##### Main Principles
* Treat commit history as a form of documentation
* Group related changes together
* Avoid combining unrelated changes into one commit
* Communicate intent and context of the change not just what changed

##### Workflow
1. Create a new branch from master with a short descriptive name (e.g., config_parsing_ability)
   `git checkout -b config_parsing_ability`
2. Do your feature development / bug fix on this branch
3. Commit your changes to local branch
4. Pull changes from main branch into your branch with rebase (see Note below)
5. Push your branch to Bitbucket using `git push`
6. Create pull request from Bitbucket
7. Pull request is reviewed and approved by another developer
8. Changes are merged into `master` and feature branch deleted

*NOTE: Since new feature development is likely to take longer than a bug fix it becomes important to keep your branch in sync with the main branch to minimize the pain of one massive merge at the end. So make sure to run `git pull --rebase origin master` every so often in your feature branch.*

